#!/usr/bin/env python3

import http.client
import json
import requests
import calendar
import sys
year = 2020
month = 9
starTime = 111111
headers = {'content-type': "application/json",'cache-control': "no-cache"}

try:
    starTime = int( sys.argv[1] )
except IndexError:
    starTime = 7

print ("sprawdam %d - dniowe"%starTime)

def generateDatesList(diff):
    y=year
    m=month
    array = []
    begin, end = calendar.monthrange(y,m)
    for d in range(end - diff):
        first = "%d-%02d-%02d"%(y,m, d+1 )
        last  ="%d-%02d-%02d"%(y,m, d+ diff + 1)
        array.append((first,last))
    return array

def searchResort(resortId):
    payload = {}
    with open('./payload.json') as json_file:
        mypayload = json.load(json_file)
    mypayload['resort'] = resortId
    for date_from,date_to in generateDatesList(starTime):
        mypayload['date_from'] = date_from
        mypayload['date_to'] = date_to
        url = "https://holidaypark.pl/api/rent/check-reservation/"
        payload = json.dumps( mypayload )
        response = requests.request("POST", url, data=payload, headers=headers)
        data = response.json()
        if data['reservation_status'] == "available":
            # print( date_from, date_to, data['reservation_status'])
            showAwaibleApartments(resortId, date_from, date_to)

def showAwaibleApartments(resortId, firs, last):
    url = 'https://holidaypark.pl/api/rent/available-apartments/'
    payload = '{"accommodation": 1, "resort": %d, "date_from": "%s", "date_to": "%s"}'%(resortId, firs, last)
    response = requests.request("POST", url, data=payload, headers=headers)
    data = response.json()
    print ( "Znaleziono %s w zakesie dat %s - %s "%(len(data['apartments']), firs, last))

def getResorts():
    url = "https://holidaypark.pl/api/rent/defaults/"

    payload = ""
    response = requests.request("GET", url, data=payload, headers=headers)
    data = response.json()

    for resort in data['resorts']:
        print ("Przeglądam  %s [%s]"% (resort['name'] , resort['pk']))
        searchResort(resort['pk'])

getResorts()
