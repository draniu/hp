#!/usr/bin/env python3
import argparse
import json
import requests
import datetime
import telegram_send
import logging



def main():

    level = logging.INFO
    fmt = '[%(levelname)s] %(asctime)s %(message)s'
    logging.basicConfig(level=level, format=fmt)

    parser = argparse.ArgumentParser(description='Holiday Park termin finder')
    parser.add_argument('date', type=str, help="Start date format yyyy-mm-dd")
    # parser.add_argument('-f', '--date-format', type=str, default='yyyy-mm-dd' , help="Date format yyyy-mm-dd ")
    parser.add_argument('-l', '--lenght', type=int, default=7 , help="default 7")
    parser.add_argument('-r', '--range', type=int, default=31 , help="default 30")
    parser.add_argument('-t', '--resort', type=int, default=0 , help="default all it can take more than one vriable eg. 1 5 8 ", nargs='+')
    parser.add_argument('-n', '--notify', type=str, default='' , help="default none option for telegram ")

    args = parser.parse_args()
    begin = datetime.date.fromisoformat(args.date)

    headers = {'content-type': "application/json",'cache-control': "no-cache"}

    message_buffer = ''

    def send_info(message):
        logging.info(message)
        file = 'prev_message.tmp'
        with open(file, 'r', encoding='utf-8') as f:
            prev = f.read()
            logging.info(prev)
            if message != prev :
                if args.notify == "telegram":
                    with open(file, 'w') as w:
                        telegram_send.send(messages=[message])
                        w.write(message)

    def generate_dates_list():
        array = []
        diff = args.lenght
        for i in range (args.range):
            b = begin + datetime.timedelta(days=i)
            e = begin + datetime.timedelta(days=diff) + datetime.timedelta(days=i)
            array.append( ( b , e ) )
        return array

    def search_resort(resortId):
        payload = {}
        with open('./payload.json') as json_file:
            mypayload = json.load(json_file)
        mypayload['resort'] = resortId
        for date_from,date_to in generate_dates_list():
            mypayload['date_from'] = str(date_from)
            mypayload['date_to']   = str(date_to)
            url = "https://holidaypark.pl/api/rent/check-reservation/"
            payload = json.dumps( mypayload )
            response = requests.request("POST", url, data=payload, headers=headers)
            data = response.json()
            if data['reservation_status'] == "available":
                return show_awaible_apartments(resortId, date_from, date_to)
        return ''


    def show_awaible_apartments(resortId, first, last):
        url = 'https://holidaypark.pl/api/rent/available-apartments/'
        payload = '{"accommodation": 1, "resort": %d, "date_from": "%s", "date_to": "%s"}'%(resortId, first, last)
        response = requests.request("POST", url, data=payload, headers=headers)
        data = response.json()
        message = f"Znaleziono { len(data['apartments']) } w {resorts[resortId]['name']} w zakesie dat {first} - {last}"
        logging.info( message )
        # Wywalić do funklcji
        return message+"\n"

    def get_resorts():
        url = "https://holidaypark.pl/api/rent/defaults/"
        payload = ""
        response = requests.request("GET", url, data=payload, headers=headers)
        return filterResorts(response.json())

    def filterResorts(data):
        formated = {}
        for resort in data['resorts']:
            formated[resort['pk']] = resort
        return formated


    resorts = get_resorts()
    logging.info(args.resort)
    if args.resort :
        for r in args.resort:
            message_buffer += search_resort(r)
    else :
        for key in resorts:
            logging.info(f"Przeglądam  {resorts[key]['name']} [{key}]")
            message_buffer += search_resort(key)

    send_info(message_buffer)

if __name__ == '__main__':
    main()
